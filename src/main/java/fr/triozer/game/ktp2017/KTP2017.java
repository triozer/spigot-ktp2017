package fr.triozer.game.ktp2017;

import fr.triozer.api.database.mysql.Table;
import fr.triozer.api.game.manager.GameManager;
import fr.triozer.api.plugin.TrioPlugin;
import fr.triozer.api.plugin.configuration.BaseConfiguration;
import fr.triozer.game.ktp2017.listener.*;
import fr.triozer.game.ktp2017.teleport.SignTeleporter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Cédric / Triozer
 */
public final class KTP2017 extends TrioPlugin {

    public static  Table   GAME;
    private static KTP2017 instance;

    private List<SignTeleporter> _temp;

    public static KTP2017 getInstance() {
        return instance;
    }

    @Override
    protected void initSQL() {
       /* GAME = TrioCore.getInstance().getTrioDatabase()
                .create("users",
                        "`id` int NOT NULL AUTO_INCREMENT", "`uuid` VARCHAR(50) NOT NULL",
                        "PRIMARY KEY (`id`)", "UNIQUE INDEX `uuid` (`uuid`)");
        */
    }

    @Override
    protected void enable() {
        instance = this;
        this._temp = new ArrayList<>();

        this.configuration = new BaseConfiguration(this);
        this.configuration.init();
        this.gameManager = new GameManager();

        YamlConfiguration signs = this.configuration.get("save.sign");

        if (signs != null) {
            signs.getKeys(false).forEach(key -> {
                ConfigurationSection section = signs.getConfigurationSection(key);

                String world = section.getString("world");
                String map   = section.getString("to");
                int    x     = section.getInt("x");
                int    y     = section.getInt("y");
                int    z     = section.getInt("z");

                Block at = Bukkit.getWorld(world).getBlockAt(x, y, z);

                if (at == null || !(at.getType() == Material.SIGN || at.getType() == Material.WALL_SIGN || at.getType() == Material.SIGN_POST)) {
                    this.console.warning("The block at world=" + world + ", x=" + x + ", y=" + y + ", z=" + z + " is not a sign.");
                } else {
                    new SignTeleporter(this, KTP2017Game.KTP_2017, map).setSign((Sign) at.getState());
                    this.console.fine("Successfully loaded teleporter for the map " + map + " at world=" + world + ", x=" + x + ", y=" + y + ", z=" + z + ".");
                }
            });
        }
    }

    @Override
    protected void disable() {
        YamlConfiguration signs = this.configuration.get("save.sign");

        if (signs != null) this.configuration.remove("save.sign");

        signs = this.configuration.create("save.sign");

        List<SignTeleporter> teleporters = new ArrayList<>();
        for (int i = 0; i < this.gameManager.getTeleportersList().size(); i++) {
            SignTeleporter teleporter = (SignTeleporter) this.gameManager.getTeleportersList().get(i);

            if (teleporters.stream()
                    .filter(_teleporter -> _teleporter.getMapName().equalsIgnoreCase(teleporter.getMapName())
                            && _teleporter.getSign().getLocation().equals(teleporter.getSign().getLocation()))
                    .count() > 0) continue;

            teleporters.add(teleporter);

            signs.set(i + ".world", teleporter.getSign().getWorld().getName());
            signs.set(i + ".to", teleporter.getMapName());
            signs.set(i + ".x", teleporter.getSign().getLocation().getX());
            signs.set(i + ".y", teleporter.getSign().getLocation().getY());
            signs.set(i + ".z", teleporter.getSign().getLocation().getZ());
        }

        this.configuration.save("save.sign");
        _temp.clear();
    }

    @Override
    protected void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new SignListener(), this);

        Bukkit.getPluginManager().registerEvents(new PlayerJoin(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerChat(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerDeath(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerQuit(), this);
    }

    public void add(SignTeleporter signTeleporter) {
        this._temp.add(signTeleporter);
    }

    public void remove(SignTeleporter signTeleporter) {
        this._temp.remove(signTeleporter);
    }

}
