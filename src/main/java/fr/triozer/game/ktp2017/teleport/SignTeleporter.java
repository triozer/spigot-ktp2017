package fr.triozer.game.ktp2017.teleport;

import fr.triozer.api.event.game.PlayerChangedWorldEvent;
import fr.triozer.api.game.Game;
import fr.triozer.api.game.Games;
import fr.triozer.api.game.teleporter.GameTeleporter;
import fr.triozer.api.game.teleporter.queue.GameQueue;
import fr.triozer.api.i18n.I18N;
import fr.triozer.api.plugin.TrioPlugin;
import fr.triozer.api.user.User;
import fr.triozer.api.util.ZipUtils;
import fr.triozer.core.TrioCore;
import fr.triozer.game.ktp2017.KTP2017;
import fr.triozer.game.ktp2017.KTP2017Game;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @author Cédric / Triozer
 */
public class SignTeleporter implements GameTeleporter {

    protected final TrioPlugin plugin;

    private final String    ID;
    private final GameQueue queue;
    private final Games     game;

    private boolean active;
    private boolean direct;

    private Sign   sign;
    private String map;

    private   World world;
    protected Game  currentGame;

    public SignTeleporter(TrioPlugin plugin, Games game, String map) {
        this.plugin = plugin;
        this.game = game;
        this.ID = UUID.randomUUID().toString().substring(0, 5);
        this.map = map;
        this.queue = new GameQueue(this);
        this.active = true;
        this.direct = false;

        this.plugin.getGameManager().add(this);
    }

    @Override
    public void add(Player player) {
        User user = TrioCore.getInstance().getPlayers().getUser(player);

        if (this.active) {
            this.plugin.getGameManager().getTeleporters()
                    .filter(teleporter -> teleporter.getQueue().contains(player))
                    .findFirst().ifPresent(teleporter -> teleporter.remove(player));

            this.queue.add(player);
            update();

            if (this.direct)
                if (this.queue.getSize() <= getProperty().getMaxPlayers()) {
                    PlayerChangedWorldEvent event = new PlayerChangedWorldEvent(this.currentGame, player, player.getWorld(), PlayerChangedWorldEvent.Reason.JOIN);
                    Bukkit.getPluginManager().callEvent(event);

                    if (event.isCancelled()) return;

                    user.sendTranslatedMessage("teleporter.teleportation", this.ID);
                    player.teleport(world.getSpawnLocation());
                } else {
                    this.active = false;
                    user.sendTranslatedMessage("queue.full", this.ID);
                }

            if (!this.direct && this.queue.getSize() == getProperty().getMinPlayers()) {
                // this.queue.players().forEach(players ->
                // TrioCore.getInstance().players().getUser(player).sendTranslatedMessage("teleporter.create", ID));

                try {
                    ZipUtils.extract(new File(this.plugin.getDataFolder(), "/maps/" + this.map + ".zip"),
                            new File(this.plugin.getDataFolder().getParentFile().getParentFile(), this.ID));
                } catch (IOException e) {
                    TrioCore.getInstance().getConsole().stacktrace("Can't extract map from maps/" + map + "! Using default map", e);
                    try {
                        ZipUtils.extract(new File(this.plugin.getDataFolder(), "/maps/Default.zip"),
                                new File(this.plugin.getDataFolder().getParentFile().getParentFile(), this.ID));
                    } catch (IOException e1) {
                        TrioCore.getInstance().getConsole().stacktrace("Where is the default map ?", e);
                        return;
                    }
                }

                KTP2017.getInstance().add(this);
                this.world = Bukkit.createWorld(new WorldCreator(this.ID));
                this.world.setAutoSave(false);
                KTP2017.getInstance().remove(this);

                createGame(world);

                for (Player players : this.queue.getPlayers()) {
                    PlayerChangedWorldEvent event = new PlayerChangedWorldEvent(this.currentGame, players, players.getWorld(), PlayerChangedWorldEvent.Reason.JOIN);
                    Bukkit.getPluginManager().callEvent(event);

                    if (event.isCancelled()) return;

                    TrioCore.getInstance().getPlayers().getUser(players).sendTranslatedMessage("teleporter.teleportation", this.ID);
                    players.teleport(this.world.getSpawnLocation());
                }

                this.direct = true;
            } else if (this.queue.getSize() == getProperty().getMaxPlayers()) {
                this.active = false;
                user.sendTranslatedMessage("queue.full", this.ID);
            }
        } else {
            user.sendTranslatedMessage("teleporter.wait");
        }
    }

    @Override
    public void update() {
        I18N language = TrioCore.getInstance().getSettings().getLanguage();

        sign.setLine(0, language.translate("teleporter.sign.0", this.game.getName()));
        sign.setLine(1, language.translate("teleporter.sign.1", this.map));
        sign.setLine(2, language.translate("teleporter.sign.2",
                String.valueOf(this.queue.getSize()), String.valueOf(getProperty().getMaxPlayers())));
        sign.setLine(3, language.translate("teleporter.sign.3", this.ID));

        sign.update(true);
    }

    @Override
    public final void remove(Player player) {
        this.queue.remove(player);
        update();
    }

    @Override
    public void destroy() {
        this.plugin.getGameManager().remove(this);
        this.queue.destroy();
        sign.getBlock().setType(Material.AIR);
    }

    @Override
    public void rebuild() {
        this.plugin.getGameManager().remove(this);
        this.queue.destroySilent();

        SignTeleporter teleporter = new SignTeleporter(this.plugin, this.game, this.map);
        teleporter.setSign(this.sign);
    }

    @Override
    public void createGame(World world) {
        this.currentGame = new KTP2017Game(world, this);
        this.plugin.getGameManager().add(this.currentGame);
    }

    @Override
    public void disable() {
        this.active = false;

        sign.setLine(0, TrioCore.getInstance().getSettings().getLanguage().translate("teleporter.sign.0", this.game.getName()));
        sign.setLine(1, "WAIT...");
        sign.setLine(2, "WAIT...");
        sign.setLine(3, "WAIT...");

        sign.update(true);
    }

    public String getGameName() {
        return this.game.getName();
    }

    @Override
    public final Game getCurrentGame() {
        return this.currentGame;
    }

    @Override
    public final GameQueue getQueue() {
        return this.queue;
    }

    @Override
    public final String getID() {
        return this.ID;
    }

    @Override
    public final String getMapName() {
        return this.map;
    }

    @Override
    public final Property getProperty() {
        return new Property(2, 16);
    }

    @Override
    public final boolean isActive() {
        return this.active;
    }

    public final Sign getSign() {
        return this.sign;
    }

    public void setSign(Sign sign) {
        this.sign = sign;
        this.plugin.getGameManager().add(this.ID, sign);
        update();
    }

}
