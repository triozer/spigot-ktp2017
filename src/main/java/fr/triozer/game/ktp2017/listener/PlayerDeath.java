package fr.triozer.game.ktp2017.listener;

import fr.triozer.api.event.game.PlayerChangedWorldEvent;
import fr.triozer.api.game.Game;
import fr.triozer.api.user.User;
import fr.triozer.core.TrioCore;
import fr.triozer.game.ktp2017.KTP2017;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * @author Triozer
 */
public class PlayerDeath implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Game game = KTP2017.getInstance().getGameManager().getGame(event.getPlayer().getWorld());
        if (game != null) {
            if (event.getItem() != null && event.getItem().getType() == Material.BED && !game.isStarted()) {
                User user = TrioCore.getInstance().getPlayers().getUser(event.getPlayer());

                PlayerChangedWorldEvent changedWorldEvent =
                        new PlayerChangedWorldEvent(game, user.getPlayer(), user.getPlayer().getWorld(), PlayerChangedWorldEvent.Reason.QUIT);
                Bukkit.getPluginManager().callEvent(changedWorldEvent);

                if (event.isCancelled()) return;

                event.getPlayer().teleport(Bukkit.getWorld("world").getSpawnLocation());
            }
        }
    }

    @EventHandler
    public void onHit(EntityDamageEvent event) {
        Game game = KTP2017.getInstance().getGameManager().getGame(event.getEntity().getWorld());
        if (game != null) {
            if (game.isStarted()) {
                event.setDamage(2.0D);
            } else {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();

        event.setDeathMessage(null);

        Game game = KTP2017.getInstance().getGameManager().getGame(event.getEntity().getWorld());
        if (game != null && game.isStarted()) {
            User user = TrioCore.getInstance().getPlayers().getUser(player);

            game.getActive().remove(user.getPlayer());
            int playerCount = game.getActive().size();

            if (player.getKiller() != null) {
                Player killer = player.getKiller();

                game.broadcastTranslatedCore("game.player.death.killed", player.getName(), killer.getName(), "" + playerCount, "" + game.getConnected());

                game.increment(killer);
            } else {
                game.broadcastTranslatedCore("game.player.death.other", player.getName(), "" + playerCount, "" + game.getConnected());
            }

            game._check();
        } else {
            for (Player players : Bukkit.getOnlinePlayers()) {
                if (!TrioCore.getInstance().getPlayers().getUser(player).isInGame()) {
                    players.sendMessage(event.getDeathMessage());
                }
            }
        }
    }

}
