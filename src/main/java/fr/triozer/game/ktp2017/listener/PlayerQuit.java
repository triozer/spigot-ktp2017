package fr.triozer.game.ktp2017.listener;

import fr.triozer.api.event.game.PlayerChangedWorldEvent;
import fr.triozer.api.game.Game;
import fr.triozer.core.TrioCore;
import fr.triozer.game.ktp2017.KTP2017;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author Triozer
 */
public class PlayerQuit implements Listener {

    @EventHandler
    public void onDisconnect(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        if (KTP2017.getInstance().getGameManager().getGame(player.getWorld()) != null) {
            event.setQuitMessage(null);
            check(KTP2017.getInstance().getGameManager().getGame(player.getWorld()), player);
        }
    }


    @EventHandler
    public void onLeave(PlayerChangedWorldEvent event) {
        Player player = event.getPlayer();

        if (event.getReason() == PlayerChangedWorldEvent.Reason.QUIT) {
            check(event.getGame(), player);
        }
    }

    private void check(Game game, Player player) {
        if (game == null) return;

        game.players().remove(TrioCore.getInstance().getPlayers().getUser(player));
        game.remove(TrioCore.getInstance().getPlayers().getUser(player));

        int playerCount = game.getActive().size();
        if (game.isStarted()) {
            if (player.getGameMode() == GameMode.ADVENTURE) {
                game.broadcastTranslatedCore("game.player.quit-in-game", player.getName(), "" + playerCount, "" + game.getConnected());

                game._check();
            }
        } else {
            game.broadcastTranslatedCore("game.player.quit", player.getName(), "" + playerCount, "" + game.getConnected());
        }

        game._empty();

        player.getInventory().clear();

        player.setGameMode((GameMode) game.getBackup().get(player.getName() + ".gamemode").getValue());
        player.setExp((Float) game.getBackup().get(player.getName() + ".xp").getValue());
        player.setLevel((Integer) game.getBackup().get(player.getName() + ".level").getValue());
        player.setCustomName((String) game.getBackup().get(player.getName() + ".name").getValue());
        player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue((Double) game.getBackup().get(player.getName() + ".max-health").getValue());
        player.setHealth((Double) game.getBackup().get(player.getName() + ".health").getValue());
        player.setFoodLevel((Integer) game.getBackup().get(player.getName() + ".food").getValue());

        ItemStack[] contents = (ItemStack[]) game.getBackup().get(player.getName() + ".inventory").getValue();
        for (ItemStack content : contents) {
            if (content == null || content.getType() == Material.AIR) continue;
            player.getInventory().setContents(contents);
        }

        /*
        if (player.isOnline())
            player.teleport((Location) game.getBackup().get(player.getName() + ".location").getValue());
        */
    }

}
