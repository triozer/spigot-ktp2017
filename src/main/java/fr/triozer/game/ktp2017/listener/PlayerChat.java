package fr.triozer.game.ktp2017.listener;

import fr.triozer.api.game.Game;
import fr.triozer.core.TrioCore;
import fr.triozer.game.ktp2017.KTP2017;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * @author Cédric / Triozer
 */
public class PlayerChat implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();

        event.getRecipients().clear();

        Game game = KTP2017.getInstance().getGameManager().getGame(event.getPlayer().getWorld());

        if (game != null) {
            event.setFormat("§7" + event.getPlayer().getName() + " §8» §7" + event.getMessage());
            event.getRecipients().addAll(game.getPlayers());
        } else {
            for (Player players : Bukkit.getOnlinePlayers()) {
                if (!TrioCore.getInstance().getPlayers().getUser(player).isInGame()) {
                    event.getRecipients().add(players);
                }
            }
        }
    }

}