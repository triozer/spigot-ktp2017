package fr.triozer.game.ktp2017.listener;

import fr.triozer.api.event.game.PlayerChangedWorldEvent;
import fr.triozer.api.game.Game;
import fr.triozer.api.property.FileProperty;
import fr.triozer.core.TrioCore;
import fr.triozer.game.ktp2017.KTP2017;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * @author Triozer
 */
public class PlayerJoin implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        Game game = KTP2017.getInstance().getGameManager().getGame(player.getWorld());
        if (game != null) {
            event.setJoinMessage(null);

            check(game, player, player.getLocation());
        }
    }

    @EventHandler
    public void onPlayerEnter(PlayerChangedWorldEvent event) {
        Player player = event.getPlayer();

        if (event.getReason() == PlayerChangedWorldEvent.Reason.JOIN)
            check(event.getGame(), player, event.getFrom().getSpawnLocation());
    }

    private void check(Game game, Player player, Location from) {
        game.getBackup().set(player.getName() + ".gamemode", new FileProperty.Value(player.getGameMode()));
        game.getBackup().set(player.getName() + ".xp", new FileProperty.Value(player.getExp()));
        game.getBackup().set(player.getName() + ".level", new FileProperty.Value(player.getLevel()));
        game.getBackup().set(player.getName() + ".name", new FileProperty.Value(player.getCustomName()));
        game.getBackup().set(player.getName() + ".max-health", new FileProperty.Value(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue()));
        game.getBackup().set(player.getName() + ".health", new FileProperty.Value(player.getHealth()));
        game.getBackup().set(player.getName() + ".food", new FileProperty.Value(player.getFoodLevel()));
        game.getBackup().set(player.getName() + ".inventory", new FileProperty.Value(player.getInventory().getContents()));
        game.getBackup().set(player.getName() + ".location", new FileProperty.Value(from));

        // player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(20.0D);
        // player.setHealth(20.0);
        // player.setHealthScaled(false);
        // player.setFoodLevel(20);
        // player.setSaturation(20);
        // player.setLevel(0);
        // player.getInventory().clear();
        // player.getInventory().setItem(8, new ItemBuilder(Material.BED).name("§cBack to hub.").build());

        Bukkit.broadcastMessage(game.getBackup().toString());

        if (game.isStarted()) {
            player.setGameMode(GameMode.SPECTATOR);

            TrioCore.getInstance().getPlayers().getUser(player).sendTranslatedMessage("game.player.spectate", player.getName());
        } else {
            player.setGameMode(GameMode.ADVENTURE);

            game.add(TrioCore.getInstance().getPlayers().getUser(player));
            int playerCount = game.getPlayers().size();

            game.broadcastTranslatedCore("game.player.join", player.getName(), "" + playerCount, "" + game.getMaxPlayers());

            if (playerCount == game.getMinPlayers())
                game._start();
        }
    }

}