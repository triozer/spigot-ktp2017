package fr.triozer.game.ktp2017.listener;

import fr.triozer.api.game.teleporter.GameTeleporter;
import fr.triozer.api.i18n.I18N;
import fr.triozer.core.TrioCore;
import fr.triozer.game.ktp2017.KTP2017;
import fr.triozer.game.ktp2017.KTP2017Game;
import fr.triozer.game.ktp2017.teleport.SignTeleporter;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

/**
 * @author Cédric / Triozer
 */
public class SignListener implements Listener {
    @EventHandler
    public void create(SignChangeEvent event) {
        if (!event.getPlayer().hasPermission("triocore.teleporter.create")) return;

        if ("[KTP]".equalsIgnoreCase(event.getLine(0))) {
            SignTeleporter teleporter;

            String map = event.getLine(1);
            if (map != null && !map.isEmpty())
                teleporter = new SignTeleporter(KTP2017.getInstance(), KTP2017Game.KTP_2017, map);
            else
                teleporter = new SignTeleporter(KTP2017.getInstance(), KTP2017Game.KTP_2017, "Default");

            Sign sign = (Sign) event.getBlock().getState();

            I18N language = TrioCore.getInstance().getSettings().getLanguage();

            event.setLine(0, language.translate("teleporter.sign.0", teleporter.getGameName()));
            event.setLine(1, language.translate("teleporter.sign.1", teleporter.getMapName()));
            event.setLine(2, language.translate("teleporter.sign.2",
                    String.valueOf(teleporter.getQueue().getSize()), String.valueOf(teleporter.getProperty().getMaxPlayers())));
            event.setLine(3, language.translate("teleporter.sign.3", teleporter.getID()));

            sign.update(true);
            teleporter.setSign((Sign) event.getBlock().getState());

            TrioCore.getInstance().getPlayers().getUser(event.getPlayer()).sendTranslatedMessage("teleporter.create",
                    teleporter.getGameName());
        }

    }

    @EventHandler
    public void click(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Block  block  = event.getClickedBlock();

        if (event.getHand() != EquipmentSlot.HAND) return;
        if (block == null) return;
        if (!(block.getType() == Material.SIGN || block.getType() == Material.WALL_SIGN || block.getType() == Material.SIGN_POST))
            return;

        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            GameTeleporter teleporter = KTP2017.getInstance().getGameManager().getTeleporter((Sign) block.getState());
            if (teleporter != null) {
                // String id = ChatColor.stripColor(sign.getLine(3).split("#")[1]);

                if (teleporter.isActive())
                    if (teleporter.getQueue().getPlayers().contains(player))
                        teleporter.remove(player);
                    else
                        teleporter.add(player);
            }
        }
    }

    @EventHandler
    public void destroy(BlockBreakEvent event) {
        Block block = event.getBlock();

        if (!(block.getType() == Material.SIGN || block.getType() == Material.WALL_SIGN || block.getType() == Material.SIGN_POST))
            return;

        GameTeleporter teleporter = KTP2017.getInstance().getGameManager().getTeleporter((Sign) block.getState());
        if (teleporter != null) {
            teleporter.destroy();
        }

    }

}
