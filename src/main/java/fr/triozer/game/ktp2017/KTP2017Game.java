package fr.triozer.game.ktp2017;

import fr.triozer.api.game.Game;
import fr.triozer.api.game.Games;
import fr.triozer.api.game.description.GameDescription;
import fr.triozer.api.game.teleporter.GameTeleporter;
import fr.triozer.api.ui.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Cédric / Triozer
 */
public class KTP2017Game extends Game {

    public final static Games KTP_2017 = new Games("ktp2017", "KTP 2017");

    public KTP2017Game(World world, GameTeleporter teleporter) {
        super(KTP2017.getInstance(), new GameDescription.Builder("KTP 2017")
                .author("Cédric / Triozer")
                .description("KTP2017 flm")
                .icon(new ItemBuilder(Material.DIAMOND).name("§6KTP 2017")
                        .lore("nice game")
                        .build())
                .version("53")
                .build(), teleporter, world);
    }

    public KTP2017Game(GameTeleporter teleporter) {
        this(Bukkit.getWorld("world"), teleporter);
    }

    @Override
    protected void init() {

    }

    @Override
    protected void update() {

    }

    @Override
    protected void start() {
        for (Player player : getActive()) {
            player.setHealth(2.0D);
            player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(2.0D);
        }

        int timeForHide = 5;

        broadcast("");
        broadcastTranslated("game.start", String.valueOf(timeForHide));
        broadcast("");

        getActive().forEach(player -> player
                .addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 20 * timeForHide, 1), true));

        new BukkitRunnable() {
            @Override
            public void run() {
                getActive().forEach(player -> player.removePotionEffect(PotionEffectType.INVISIBILITY));

                broadcast("");
                broadcastTranslated("game.play");
                broadcast("");
            }
        }.runTaskLater(KTP2017.getInstance(), 20L * timeForHide);
    }

    @Override
    protected void finish() {
        List<Map.Entry<Player, Integer>> list = new ArrayList<>();

        active.forEach(player -> {
            list.add(new AbstractMap.SimpleEntry<>(player.getPlayer()
                    , (Integer) player.getProperty().get("game." + teleporter.getID() + ".kills").getValue()));
        });

        list.sort((o1, o2) -> (o2.getValue()).compareTo(o1.getValue())); // LAMBDA IS TO EASY XD

        Player player = active.get(0).getPlayer();
        int    x      = 0;

        broadcast("");
        broadcastTranslated("game.winner", player.getName());
        broadcast("");
        broadcast(prefix + "§6Classement : ");
        broadcast("");

        for (Map.Entry<Player, Integer> entry : list) {
            x++;

            switch (x) {
                case 1:
                    broadcast("§6" + x + ". " + entry.getKey().getName() + " (" + entry.getValue() + ")");
                    break;
                case 2:
                    broadcast("§e" + x + ". " + entry.getKey().getName() + " (" + entry.getValue() + ")");
                    break;
                case 3:
                    broadcast("§c" + x + ". " + entry.getKey().getName() + " (" + entry.getValue() + ")");
                    break;
                default:
                    broadcast("§7" + x + ". " + entry.getKey().getName() + " (" + entry.getValue() + ")");
                    break;
            }
        }

        broadcast("");
    }
}
